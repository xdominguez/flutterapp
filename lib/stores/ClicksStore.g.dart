// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ClicksStore.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ClicksStore on _ClicksStore, Store {
  final _$clicksAtom = Atom(name: '_ClicksStore.clicks');

  @override
  int get clicks {
    _$clicksAtom.reportRead();
    return super.clicks;
  }

  @override
  set clicks(int value) {
    _$clicksAtom.reportWrite(value, super.clicks, () {
      super.clicks = value;
    });
  }

  final _$_ClicksStoreActionController = ActionController(name: '_ClicksStore');

  @override
  void increment() {
    final _$actionInfo = _$_ClicksStoreActionController.startAction(
        name: '_ClicksStore.increment');
    try {
      return super.increment();
    } finally {
      _$_ClicksStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
clicks: ${clicks}
    ''';
  }
}
