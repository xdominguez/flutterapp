import 'package:mobx/mobx.dart';

part 'ClicksStore.g.dart';

class ClicksStore = _ClicksStore with _$ClicksStore;

abstract class _ClicksStore with Store {

  @observable
  int clicks = 0;

  @action
  void increment() => clicks = clicks + 1;
}